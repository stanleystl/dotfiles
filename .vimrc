filetype plugin indent on

" show existing tab with 4 spaces width
set ts=4 sts=4 sw=4 noexpandtab

" Source the vimrc file after saving it
if has("autocmd")
  autocmd bufwritepost ~/.vimrc source $MYVIMRC
endif
